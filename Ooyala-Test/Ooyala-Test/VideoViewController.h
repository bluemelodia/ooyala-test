//
//  VideoViewController.h
//  Ooyala-Test
//
//  Created by Melanie Lislie Hsu on 8/20/15.
//  Copyright (c) 2015 Melanie Lislie Hsu. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OOOoyalaPlayerViewController;

@interface VideoViewController : UIViewController

@property (nonatomic, strong) OOOoyalaPlayerViewController *ooyalaPlayerViewController;
@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (weak, nonatomic) IBOutlet UITextView *textView;

- (id)initWithEmbedCode:(NSString *)eCode playerCode:(NSString *)pCode andDomain:(NSString *)domain;

@end
